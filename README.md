# How to install nextcloud, a self hosted cloud solution?

What is nextcloud? See https://nextcloud.com

## Use Case
As a member of a small organization (5 to 20 people) I want to share documents (approx. 10 GB) with my colleagues. Every team member shall be able to access the documents for reading. A few colleagues shall have the authorization to create, read, update and delete documents. An automated backup mechanism is required.  SSL/TLS encryption is mandatory.

## Nextcloud and Docker
The german computer magazine c't (https://www.heise.de/ct/) published an article (c't 4/2018, pp. 88) about docker containers and SSL/TLS certificates. It explains how to obtain certificates from letsencrypt.org for your application in an automated way. Based on this article I built a docker-compose script for a nextcloud service. It uses MariaDB instead of mysql, nginx as reverse proxy and a companion to automatically fetch SSL/TLS certificates from letsencrypt.org .

The file docker-compose.yml is a template for a nextcloud installation. You need to adapt the file for your specific situation. You'll need:
* a host, connected to the internet
* a domain (own domain or dynamic dns), pointing to your host

My setup:
* Cloud server: https://www.hetzner.de/cloud
* OS: CentOS Linux release 7.5.1804 (Core)

Get the server up and running, then logon and install docker and docker-compose:
$ yum -y update
$ yum -y install docker docker-registry
$ systemctl enable docker.service
$ systemctl start docker.service

Create the following directories:
/home/nc
/home/nc/certs
/home/nc/db
/home/nc/nextcloud
/home/nc/vhosts

Copy the files db.env and docker-compose.yml into /home/nc
Adapt the files according to your particular situation. In file db.env you shall specify a secret password for the nextcloud DB:
* MYSQL_PASSWORD

In file docker-compose.yml you shall specify the root password for the DB and your host and email address:
* MYSQL_ROOT_PASSWORD
* VIRTUAL_HOST
* LETSENCRYPT_HOST
* LETSENCRYPT_EMAIL

Then execute:
$ docker-compose up -d

Check with your browser that nextcloud is up and running: https://nextcloud.example.net
